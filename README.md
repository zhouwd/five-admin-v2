<p align="center">
    <a href="https://gitee.com/zhang-yuge668/five-admin-v2/">
        <img src="https://gitee.com/zhang-yuge668/five-admin-v2/widgets/widget_3.svg">
    </a>
</p>
<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">FiveAdminV2后台管理系统前端模板</h1>
<p align="center">
	<a href="https://gitee.com/zhang-yuge668/five-admin-v2/stargazers">
        <img src="https://gitee.com/zhang-yuge668/five-admin-v2/badge/star.svg?theme=dark">
    </a>
	<a href="https://gitee.com/zhang-yuge668/five-admin-v2/members">
        <img src="https://gitee.com/zhang-yuge668/five-admin-v2/badge/fork.svg?theme=dark">
    </a>
	<a href="https://gitee.com/zhang-yuge668/five-admin-v2/blob/master/LICENSE">
        <img src="https://img.shields.io/github/license/mashape/apistatus.svg">
    </a>
</p>

## 简介
项目基于 FiveAdmin 重构。

项目技术栈： JavaScript、Vue3、Vite5、Pinia、ElementPlus、Tailwindcss 等。




## 分支介绍
+   master：主分支（推荐）
+   dev：开发分支（最新代码在这里）

## 相关链接
+ 文档地址：https://zhang-yuge668.gitee.io/preview/five-admin-v2-docs/
+ 项目预览：https://zhang-yuge668.gitee.io/preview/five-admin-v2/
+ 账号/密码：admin/admin;test/test

[![zhangyuge/five-admin-v2](https://gitee.com/zhang-yuge668/five-admin-v2/widgets/widget_card.svg?colors=4183c4,ffffff,ffffff,e3e9ed,666666,9b9b9b)](https://gitee.com/zhang-yuge668/five-admin-v2)



## 项目截图

![](https://oscimg.oschina.net/oscnet/up-961a7a6f13737ff79541f31d030227df9fa.png)

![](https://oscimg.oschina.net/oscnet/up-8a7207add56914df3d7c60f6d10c4c519e2.png)

![](https://oscimg.oschina.net/oscnet/up-650c9437afa97308579ebf83400d90f6de7.png)

![]( https://oscimg.oschina.net/oscnet/up-4ecb6b6db74daec86c562d75864b58f3b47.png)

![](https://oscimg.oschina.net/oscnet/up-9950d6de99a3a3fdd3020a82763a2cf4bda.png)
